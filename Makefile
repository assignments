SCHEMES = gray.schema sargon.schema gitacl.schema umi.schema
MIBS = OWS-STATS.txt SESSION-STATS.txt
REFMIBS = TALLYMAN-MIB.txt MYSQL-STAT-MIB.txt SENDMAIL-STATS.txt HOSTPROC-MIB.txt

TEXINFO_SOURCES = $(SCHEMES:.schema=.inc) $(MIBS:.txt=.inc) $(REFMIBS:.txt=.inc)
SOURCES = gray-assignments.texi version.texi $(TEXINFO_SOURCES)

gray-assignments.info: $(SOURCES)
	makeinfo gray-assignments.texi

html: $(SOURCES)
	makeinfo --html --init-file=html.init gray-assignments.texi

version.texi: gray-assignments.texi
	@echo "@set UPDATED " \
          $$(date +"%d %B %Y" --date "$$(stat -c %y gray-assignments.texi)") > version.texi

.schema.inc:
	@sed -e 's/\([{}@]\)/@\1/g' $< > $@

.txt.inc:
	@sed -e 's/\([{}@]\)/@\1/g' $< > $@

.SUFFIXES: .schema .txt .inc

define localcopy
$(1):;
	@curl -sS -o $(1).tmp $(2)
	@if test -f $(1) && cmp $(1).tmp $(1) >/dev/null 2>&1; then \
	    rm $(1).tmp; \
	else \
	    mv $(1).tmp $(1); \
	fi
endef

.PHONY: $(REFMIBS)

$(call localcopy,SENDMAIL-STATS.txt,http://git.gnu.org.ua/cgit/netsnmp-sendmail.git/plain/SENDMAIL-STATS.txt)
$(call localcopy,MYSQL-STAT-MIB.txt,http://git.gnu.org.ua/cgit/mysqlstat.git/plain/src/MYSQL-STAT-MIB.txt)
$(call localcopy,TALLYMAN-MIB.txt,http://git.gnu.org.ua/cgit/tallyman.git/plain/src/TALLYMAN-MIB.txt)
$(call localcopy,HOSTPROC-MIB.txt,http://git.gnu.org.ua/cgit/hostproc.git/plain/HOSTPROC-MIB.txt)

install: html
	@if test -z "$(DESTDIR)"; then \
		echo >&2 "usage: make install DESTDIR=DIR"; \
		exit 1; \
	fi
	test -d $(DESTDIR) || mkdir -p $(DESTDIR)
	rsync -az --delete --exclude=build/ gray-assignments/ gray.css graphics $(DESTDIR)
